import {useEffect, useState} from 'react'

const useCdsEndpoint = (freetext, jwt) => {
  /*
    freetext: str -> Text to search
    jwt:      str -> jwt from portal for auth
  */
  const [results, setResults] = useState(null);

  const payload = {
    "filters":{},
    "freetext":freetext,
    "sortBy":"relevant",
    "contentType":"",
    "groupId":"",
    "language":"",
    "includeContentOutsideSubscription":false,
    "queryCorrection":true,
    "includeUpcomingReports":true,
    "rows": 3,
  }

  useEffect(() => {
    if (freetext === '' || jwt === '') return;

    fetch("/api-v2/search", {
      method: "POST",
      headers: new Headers({
        "Authorization": `Bearer ${jwt}`
      }),
      body: JSON.stringify(payload),
      }).then((result) => result.json().then(json => setResults(json)))
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [freetext, jwt]);

  return results
}

export default useCdsEndpoint;