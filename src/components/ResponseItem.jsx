const ResponseItem = ({ title, subtitle, url, dateStr }) => {
  return (
    <div className="Card">
      <h2><a href={url}>{title}</a></h2>
      <h4>{dateStr}</h4>
      <h3>{subtitle}</h3>
    </div>
  )
}

export default ResponseItem