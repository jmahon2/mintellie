import { useRef } from "react";

const InputForm = ({submitHandler}) => {
    const inputRef = useRef(null);

    return (
        <div>
            <input ref={inputRef} placeholder="Enter your JWT" />
            <button onClick={() => submitHandler(inputRef.current.value)}>Submit</button>
        </div>
    );
};

export default InputForm;