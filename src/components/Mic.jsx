import { useRef, useState } from "react";
import SpeechRecognition, { useSpeechRecognition } from "react-speech-recognition";
import microPhoneIcon from "../mic_icon.png";
import { Fireworks } from 'fireworks/lib/react';

let fxProps = {
    count: 3,
    interval: 200,
    colors: ['#cc3333', '#4CAF50', '#81C784'],
    calc: (props, i) => ({
      ...props,
      x: (i + 1) * (window.innerWidth / 3) - (i + 1) * 100,
      y: 200 + Math.random() * 100 - 50 + (i === 2 ? -80 : 0)
    })
  }

const Mic = ({freetextHandler}) => {
  const { transcript, resetTranscript } = useSpeechRecognition({});
  const [isListening, setIsListening] = useState(false);
  const microphoneRef = useRef(null);
  const [beforeAnythingElseFlag, setbeforeAnythingElseFlag] = useState(false);

  if (!SpeechRecognition.browserSupportsSpeechRecognition()) {
    return (
      <div className="mircophone-container">
        Browser is not Support Speech Recognition.
      </div>
    );
  }
  const handleButtonPress = () => {
    if (isListening)
    {
      stopHandle();
    }
    else
    {
      resetTranscript();
      handleListing();
    }
  }

  const handleListing = () => {
    setIsListening(true);
    microphoneRef.current.classList.add("listening");
    SpeechRecognition.startListening({
      continuous: true,
    });
  };

  const stopHandle = () => {
    setIsListening(false);
    microphoneRef.current.classList.remove("listening");
    SpeechRecognition.stopListening();

    const canIGetA = /can I get a [a-z ]*? on /gi;
    const whatDoes = /what does [a-z ]*? think of /gi;

    if (beforeAnythingElse(transcript)) { return; }
    else if (matchOnAndSearch(transcript, canIGetA)) { return; }
    else if (matchOnAndSearch(transcript, whatDoes)) { return; }
    else { pleaseRepeat(); }
  };

  const beforeAnythingElse = (transcript) => {
    const beforeAnythingTxt = /before anything else/gi;
    if (transcript.match(beforeAnythingTxt)) {
      console.log(`MINTEL, Before anything Else`)
      setbeforeAnythingElseFlag(true);
      return true;
    }
    setbeforeAnythingElseFlag(false);
    return false;
  };

  const matchOnAndSearch = (transcript, regex) => {
    if (transcript.match(regex)) {
        let search_term =  transcript.replace(regex, "")
        CDS_search(search_term);
        return true;
    }
    return false;
  };

  const CDS_search = (search_term) => {
    console.log(`Transcript: ${transcript}`)
    console.log(`Search term: ${search_term}`)
    freetextHandler(search_term);
  };

  const pleaseRepeat = () => {
    let utterThis = new SpeechSynthesisUtterance(`Sorry, could you please repeat`);
    window.speechSynthesis.speak(utterThis);
    console.log('please Repeat')
  };

  return (
    <div className="microphone-container">
      {beforeAnythingElseFlag && <Fireworks {...fxProps} />}
      <button
        className="microphone-icon-container"
        ref={microphoneRef}
        onClick={handleButtonPress}
      >
        <img src={microPhoneIcon} className="microphone-icon" alt="Microphone icon" />
      </button>
      <div className="microphone-status">
        {isListening ? "Listening........." : "Click to start Listening"}
      </div>
      <div className="microphone-result-text">"{transcript}"</div>
    </div>
  );
}

export default Mic;
