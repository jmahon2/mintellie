import './App.css';
import InputForm from './components/InputForm';
import ResponseItem from './components/ResponseItem';
import Mic from './components/Mic';
import logo from './mintellie_logo.png';
import { useRef, useState } from 'react';
import useCdsEndpoint from './useCdsEndpoint/useCdsEndpoint';

const App = () => {
  const [jwt, setJwt] = useState('');
  const [freetext, setFreetext] = useState('');
  const results = useCdsEndpoint(freetext, jwt);
  const previousResults = useRef([])

  console.log("results", results);
  console.log(results?.results);

  return (
    <div className='App'>
      <img src={logo} alt="Mintellie Logo" />
      <a href='https://qa-portal.mintel.com/portal/ae/get-jwts' target="_blank" rel="noreferrer">Get your portal jwt here!</a>
      <InputForm submitHandler={setJwt} />
      <Mic className="Mic" freetextHandler={setFreetext} />
      <div className='Cards'></div>
        {results?.results && results.results.map((item, key) => 
          <ResponseItem key={key} title={item.title} subtitle={item.sub_title} url={item.url} dateStr={item.display_date} />
        )}
        {results?.results && (()=>{
        if (results.results !== previousResults.current) {
          let utterThis = new SpeechSynthesisUtterance(`I found you ${results.results.length} results`);
          window.speechSynthesis.speak(utterThis);
          results.results.map((item, key) => {
            let utterThis = new SpeechSynthesisUtterance(item.title);
            window.speechSynthesis.speak(utterThis);
          })
          previousResults.current = results.results;
        }
      })()}
      </div>
  );
}

export default App;
